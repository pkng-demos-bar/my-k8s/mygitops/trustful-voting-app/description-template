## MR Summary

(Summarize this merge request concisely)


## Details

(insert any information e.g., code, description, logs)


### Conformity Checklist
- [ ] Changelog entry
- [ ] Documentation
- [ ] Coding standard review
- [ ] Infrastructure configurations
- [ ] [Open change request](https://gitlab.com/pkng-demos-bar/my-k8s/mygitops/trustful-voting-app/iacs/deployment-k8s/-/issues)

/cc @toshitakaito
