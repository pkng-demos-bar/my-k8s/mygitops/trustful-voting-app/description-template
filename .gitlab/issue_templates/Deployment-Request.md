## Deployment Request

<!-- Summarize the deploy concisely -->

### Release Service Version
- Vote: <!-- https://gitlab.com/pkng-demos-bar/my-k8s/mygitops/trustful-voting-app/services/vote/-/releases/x.y.z -->
- Result: <!-- https://gitlab.com/pkng-demos-bar/my-k8s/mygitops/trustful-voting-app/services/result/-/releases/x.y.z -->
- Worker: <!-- https://gitlab.com/pkng-demos-bar/my-k8s/mygitops/trustful-voting-app/services/worker/-/releases/x.y.z -->

## Targeted Env

- [ ] Dev
- [ ] SIT
- [ ] UAT
- [ ] Prod

## Affects

<!-- What subsystem will this deployment affect ->

## Relevant documentation

<!-- Paste any relevant documentation, links -->


/label ~Deployment::Requested